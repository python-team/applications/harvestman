#! /usr/bin/env python
# -- coding: utf-8

""" HarvestMan - Extensible, modular and multithreaded Internet
    spider program using urllib2 and other python modules. This is
    the main module of HarvestMan.
    
    Version      - 2.0 beta 1.

    Author: Anand B Pillai <anand at harvestmanontheweb.com>

    HARVESTMAN is free software. See the file LICENSE.txt for information
    on the terms and conditions of usage, and a DISCLAIMER of ALL WARRANTIES.

 Modification History

    Created: Aug 2003

     Jan 23 2007          Anand      Changes to copy config file to ~/.harvestman/conf
                                     folder on POSIX systems. This file is also looked for
                                     if config.xml not found in curdir.
     Jan 25 2007          Anand      Simulation feature added. Also modified config.py
                                     to allow reading cmd line arguments when passing
                                     a config file using -C option.
     Feb 7 2007          Anand       Finished implementation of plugin feature. Crawl
                                     simulator is now a plugin.
     Feb 8 2007          Anand       Added swish-e integration as a plugin.
     Feb 11 2007         Anand       Changes in the swish-e plugin implementation,
                                     by using callbacks.
     Mar 2 2007          Anand       Renamed finish to finish_project. Moved
                                     Finish method from common.py to here and
                                     renamed it as finish(...). finish is never
                                     called at project end, but by default at
                                     program end.
     Mar 7 2007          Anand       Disabled urlserver option.
     Mar 15 2007         Anand       Added bandwidth calculation for determining
                                     max filesize before crawl. Need to add
                                     code to redetermine bandwidth when network
                                     interface changes.
     Apr 18 2007         Anand       Added the urltypes module for URL type
                                     definitions and replaced entries with it.
                                     Upped version number to 2.0 since this is
                                     almost a new program now!
     Apr 19 2007         Anand       Disabled urlserver option completely. Removed
                                     all referring code from this module, crawler
                                     and urlqueue modules. Moved code for grabbing
                                     URL to new hget module.
    Apr 24 2007          Anand       Made to work on Windows (XP SP2 Professional,
                                     Python 2.5)
    Apr 24 2007          Anand       Made the config directory creation/session
                                     saver features to work on Windows also.
    Apr 24 2007          Anand       Modified connector algorithm to flush data to
                                     temp files for hget. This makes sure that hget
                                     can download huge files as multipart.
    May 7 2007           Anand       Added plugin as option in configuration file.
                                     Added ability to process more than one plugin
                                     at once. Modified loading logic of plugins.
    May 10 2007          Anand       Replaced a number of private attributes in classes
                                     (with double underscores), to semi-private (one
                                     underscore). This helps in inheriting from these
                                     classes.
    Dec 12 2007          Anand       Re-merged code from harvestmanklass module to this
                                     and moved common initialization code to appbase.py
                                     under HarvestManAppBase class.
    Feb 12-14 08        Anand        Major datastructure enhancements/revisions, fixes
                                     etc in datamgr, rules, urlparser, connector, crawler,
                                     ,urlqueue, urlthread modules.

   Copyright (C) 2004 Anand B Pillai.     
"""     

__version__ = '2.0 b1'
__author__ = 'Anand B Pillai'

import __init__
import os, sys

from shutil import copy
import cPickle, pickle
import time
import threading
import shutil
import glob
import re
import copy
import signal
import locale

from appbase import HarvestManAppBase

from lib.common.common import *
from lib.common.macros import *

from lib import urlqueue
from lib import connector
from lib import rules
from lib import datamgr
from lib import utils
from lib import urlparser

from lib.common.methodwrapper import MethodWrapperMetaClass

# Defining callback points
__callbacks__ = { 'run_saved_state_callback':'HarvestMan:run_saved_state',
                  'restore_state_callback':'HarvestMan:restore_state',
                  'run_projects_callback':'HarvestMan:run_projects',
                  'start_project_callback':'HarvestMan:start_project',
                  'finish_project_callback':'HarvestMan:finish_project',
                  'finalize_callback':'HarvestMan:finalize',                  
                  'init_callback' : 'HarvestMan:init'}

# Defining pluggable functions
__plugins__ = { 'clean_up_plugin':'HarvestMan:clean_up',
                'save_current_state_plugin': 'HarvestMan:save_current_state',
                'restore_state_plugin': 'HarvestMan:restore_state',
                'reset_state_plugin': 'HarvestMan:reset_state' }


class HarvestMan(HarvestManAppBase):
    """ Top level application class """

    klassmap = {}
    
    __metaclass__ = MethodWrapperMetaClass
    USER_AGENT = "HarvestMan v2.0"
        
    def __init__(self):
        """ Constructor """

        self._projectstartpage = 'file://'
        super(HarvestMan, self).__init__()
        
    def finish_project(self):
        """ Actions to take after download is over for the current project """
        
        # Localise file links
        # This code sits in the data manager class
        dmgr = GetObject('datamanager')
        dmgr.post_download_setup()
        
        # if not self._cfg.testing:
        if self._cfg.browsepage:
            logconsole("Creating browser index page for the project...")
            browser = utils.HarvestManBrowser()
            browser.make_project_browse_page()
            logconsole("Done.")

    def finalize(self):
        """ This function can be called at program exit or
        when handling signals to clean up """

        global RegisterObj
        
        # If this was started from a runfile,
        # remove it.
        if self._cfg.runfile:
            try:
                os.remove(self._cfg.runfile)
            except OSError, e:
                moreinfo('Error removing runfile %s.' % self._cfg.runfile)

        # inform user of config file errors
        if RegisterObj.userdebug:
            logconsole("Some errors were found in your configuration, please correct them!")
            for x in range(len(RegisterObj.userdebug)):
                logconsole(str(x+1),':', RegisterObj.userdebug[x])

        RegisterObj.userdebug = []
        logconsole('HarvestMan session finished.')

        RegisterObj.ini = 0
        RegisterObj.logger.shutdown()

    def save_current_state(self):
        """ Save state of objects to disk so program
        can be restarted from saved state """

        # If savesession is disabled, return
        if not self._cfg.savesessions:
            extrainfo('Session save feature is disabled.')
            return
        
        # Top-level state dictionary
        state = {}
        # All state objects are dictionaries

        # Get state of queue & tracker threads
        state['trackerqueue'] = GetObject('trackerqueue').get_state()
        # Get state of datamgr
        state['datamanager'] = GetObject('datamanager').get_state()
        # Get state of urlthreads 
        #p = GetObject('datamanager')._urlThreadPool
        #if p: state['threadpool'] = p.get_state()
        #state['ruleschecker'] = GetObject('ruleschecker').get_state()

        # Get config object
        #state['configobj'] = GetObject('config').copy()
        
        # Dump with time-stamp 
        fname = os.path.join(self._cfg.usersessiondir, '.harvestman_saves#' + str(int(time.time())))
        moreinfo('Saving run-state to file %s...' % fname)

        try:
            cPickle.dump(state, open(fname, 'wb'), pickle.HIGHEST_PROTOCOL)
            moreinfo('Saved run-state to file %s.' % fname)
        except (pickle.PicklingError, RuntimeError), e:
            logconsole(e)
            moreinfo('Could not save run-state !')
        
    def welcome_message(self):
        """ Print a welcome message """

        logconsole('Starting %s...' % self._cfg.progname)
        logconsole('Copyright (C) 2004, Anand B Pillai')
        logconsole(' ')

    def register_common_objects(self):
        """ Register common objects required by all projects """

        # Set myself
        SetObject(self)

        # Set verbosity in logger object
        GetObject('logger').setLogSeverity(self._cfg.verbosity)
        
        # Data manager object
        dmgr = datamgr.HarvestManDataManager()
        SetObject(dmgr)
        
        # Rules checker object
        ruleschecker = rules.HarvestManRulesChecker()
        SetObject(ruleschecker)
        
        # Connector object
        conn = connector.HarvestManNetworkConnector()
        SetObject(conn)

        # Connector factory
        conn_factory = connector.HarvestManUrlConnectorFactory(self._cfg.connections)
        SetObject(conn_factory)

        tracker_queue = urlqueue.HarvestManCrawlerQueue()
        SetObject(tracker_queue)

    def start_project(self):
        """ Start the current project """

        # crawls through a site using http/ftp/https protocols
        if self._cfg.project:
            info('*** Log Started ***\n')
            if not self._cfg.resuming:
                info('Starting project',self._cfg.project,'...')
            else:
                info('Re-starting project',self._cfg.project,'...')                

            
            # Write the project file 
            if not self._cfg.fromprojfile:
                projector = utils.HarvestManProjectManager()
                projector.write_project()

        # Make filters for rules object now only otherwise
        # it interferes with project-file writing
        GetObject('ruleschecker').make_filters()
        
        if not self._cfg.resuming:
            info('Starting download of url',self._cfg.url,'...')
        else:
            pass
            
        # Initialize datamgr
        dmgr = GetObject('datamanager')
        dmgr.initialize()
        
        # Read the project cache file, if any
        if self._cfg.pagecache:
            dmgr.read_project_cache()

        tracker_queue = GetObject('trackerqueue')

        if not self._cfg.resuming:
            # Configure tracker manager for this project
            if tracker_queue.configure():
                # start the project
                tracker_queue.crawl()
        else:
            tracker_queue.restart()

    def clean_up(self):
        """ Clean up actions to do, say after
        an interrupt """

        # Shut down logging on file
        info('Shutting down logging...')
        GetObject('logger').disableFileLogging()
        
        if self._cfg.fastmode:
            tq = GetObject('trackerqueue')
            tq.endloop()

    def calculate_bandwidth(self):
        """ Calculate bandwidth. This also sets limit on
        maximum file size """

        # Calculate bandwidth
        bw = 0
        # Look for harvestman.conf in user conf dir
        conf = os.path.join(self._cfg.userconfdir, 'harvestman.conf')
        if not os.path.isfile(conf):
            conn = connector.HarvestManUrlConnector()
            urlobj = urlparser.HarvestManUrlParser('http://harvestmanontheweb.com/schemas/HarvestMan.xsd')
            bw = conn.calc_bandwidth(urlobj)
            bwstr='bandwidth=%f\n' % bw
            if bw:
                try:
                    open(conf,'w').write(bwstr)
                except IOError, e:
                    pass
        else:
            r = re.compile(r'(bandwidth=)(.*)')
            try:
                data = open(conf).read()
                m = r.findall(data)
                if m:
                    bw = float(m[0][-1])
            except IOError, e:
                pass

        return bw
        
    def create_initial_directories(self):
        """ Create initial directories """

        # Create user's HarvestMan directory on POSIX at $HOME/.harvestman and 
        # on Windows at $USERPROFILE/Local Settings/Application Data/HarvestMan
        harvestman_dir = self._cfg.userdir
        harvestman_conf_dir = self._cfg.userconfdir
        harvestman_sessions_dir = self._cfg.usersessiondir

        if not os.path.isdir(harvestman_dir):
            try:
                logconsole('Looks like you are running HarvestMan for the first time in this machine')
                logconsole('Doing initial setup...')
                logconsole('Creating folder %s for storing HarvestMan application data...' % harvestman_dir)
                os.makedirs(harvestman_dir)
            except (OSError, IOError), e:
                logconsole(e)

        if not os.path.isdir(harvestman_conf_dir):
            try:
                logconsole('Creating "conf" sub-directory in %s...' % harvestman_dir)
                os.makedirs(harvestman_conf_dir)

                # Create user configuration in .harvestman/conf
                conf_data = self._cfg.generate_user_configuration()
                logconsole("Generating user configuration in %s..." % harvestman_conf_dir)
                try:
                    user_conf_file = os.path.join(harvestman_conf_dir, 'config.xml')
                    open(user_conf_file, 'w').write(conf_data)
                    logconsole("Done.")                    
                except IOError, e:
                    print e

            except (OSError, IOError), e:
                logconsole(e)

        if not os.path.isdir(harvestman_sessions_dir):
            try:
                logconsole('Creating "sessions" sub-directory in %s...' % harvestman_dir)
                os.makedirs(harvestman_sessions_dir)                        
                logconsole('Done.')
            except (OSError, IOError), e:
                logconsole(e)
        
    def init(self):
        """ Initialization """

        self._cfg.USER_AGENT = self.__class__.USER_AGENT

        self.register_common_objects()
        self.create_initial_directories()

        # Calculate bandwidth and set max file size
        # bw = self.calculate_bandwidth()
        # Max file size is calculated as bw*timeout
        # where timeout => max time for a worker thread
        # if bw: self._cfg.maxfilesize = bw*self._cfg.timeout

    def init_config(self):
        """ Initialize the configuration """
        
        # Following 2 methods are inherited from the parent class
        self.get_options()
        self.process_plugins()
        # For the time being, save session set to false
        self._cfg.savesessions = 0
        
    def run_projects(self):
        """ Run the HarvestMan projects specified in the config file """

        # Set locale - To fix errors with
        # regular expressions on non-english web
        # sites.
        locale.setlocale(locale.LC_ALL, '')

        if self._cfg.verbosity:
            self.welcome_message()

        for x in range(len(self._cfg.urls)):
            # Get all project related vars
            url = self._cfg.urls[x]

            project = self._cfg.projects[x]
            verb = self._cfg.verbosities[x]
            tout = self._cfg.projtimeouts[x]
            basedir = self._cfg.basedirs[x]

            if not url or not project or not basedir:
                info('Invalid config options found!')
                if not url: info('Provide a valid url')
                if not project: info('Provide a valid project name')
                if not basedir: info('Provide a valid base directory')
                continue
            
            # Set the current project vars
            self._cfg.url = url
            self._cfg.project = project
            self._cfg.verbosity = verb
            self._cfg.projtimeout = tout
            self._cfg.basedir = basedir
                
            self.run_project()
            
    def run_project(self):
        """ Run a harvestman project """

        # Set project directory
        # Expand any shell variables used
        # in the base directory.
        self._cfg.basedir = os.path.expandvars(os.path.expanduser(self._cfg.basedir))
        
        if self._cfg.basedir:
            self._cfg.projdir = os.path.join( self._cfg.basedir, self._cfg.project )
            if self._cfg.projdir:
                if not os.path.exists( self._cfg.projdir ):
                    os.makedirs(self._cfg.projdir)

        # Set message log file
        # From 1.4 version message log file is created in the
        # project directory as <projectname>.log
        if self._cfg.projdir and self._cfg.project:
            self._cfg.logfile = os.path.join( self._cfg.projdir, "".join((self._cfg.project,
                                                                          '.log')))

        SetLogFile()
        
        if not self._cfg.testnocrawl:
            #try:
            self.start_project()
            #except Exception, e:
            #    # raise
            #    self.handle_interrupts(-1, None, e)

        self.finish_project()

    def restore_state(self, state_file):
        """ Restore state of some objects from a previous run """

        try:
            state = cPickle.load(open(state_file, 'rb'))
            # This has six keys - configobj, threadpool, ruleschecker,
            # datamanager, common and trackerqueue.

            # First update config object
            localcfg = {}
            cfg = state.get('configobj')
            if cfg:
                for key,val in cfg.items():
                    localcfg[key] = val
            else:
                print 'Config corrupted'
                return RESTORE_STATE_NOT_OK

            # Now update trackerqueue
            tq = GetObject('trackerqueue')
            ret = tq.set_state(state.get('trackerqueue'))
            if ret == -1:
                logconsole("Error restoring state in 'urlqueue' module - cannot proceed further!")
                return RESTORE_STATE_NOT_OK                
            else:
                logconsole("Restored state in urlqueue module.")
            
            # Now update datamgr
            dm = GetObject('datamanager')
            ret = dm.set_state(state.get('datamanager'))
            if ret == -1:
                logconsole("Error restoring state in 'datamgr' module - cannot proceed further!")
                return RESTORE_STATE_NOT_OK                
            else:
                dm.initialize()
                logconsole("Restored state in datamgr module.")                
            
            # Update threadpool if any
            pool = None
            if state.has_key('threadpool'):
                pool = dm._urlThreadPool
                ret = pool.set_state(state.get('threadpool'))
                logconsole('Restored state in urlthread module.')
            
            # Update ruleschecker
            rules = GetObject('ruleschecker')
            ret = rules.set_state(state.get('ruleschecker'))
            logconsole('Restored state in rules module.')

            # Everything is fine, copy localcfg to config object
            for key,val in localcfg.items():
                self._cfg[key] = val

            # Open stream to log file
            SetLogFile()
                
            return RESTORE_STATE_OK
        except (pickle.UnpicklingError, AttributeError, IndexError, EOFError), e:
            return RESTORE_STATE_NOT_OK            

    def run_saved_state(self):

        # If savesession is disabled, return
        if not self._cfg.savesessions:
            extrainfo('Session save feature is disabled, ignoring save files')
            return SAVE_STATE_NOT_OK
        
        # Set locale - To fix errors with
        # regular expressions on non-english web
        # sites.
        # See if there is a file named .harvestman_saves#...
        sessions_dir = self._cfg.usersessiondir

        files = glob.glob(os.path.join(sessions_dir, '.harvestman_saves#*'))

        # Get the last dumped file
        if files:
            runfile = max(files)
            res = raw_input('Found HarvestMan save file %s. Do you want to re-run it ? [y/n]' % runfile)
            if res.lower()=='y':
                if self.restore_state(runfile) == RESTORE_STATE_OK:
                    self._cfg.resuming = True
                    self._cfg.runfile = runfile

                    if self._cfg.verbosity:
                        self.welcome_message()
        
                    if not self._cfg.testnocrawl:
                        try:
                            self.start_project()
                        except Exception, e:
                            self.handle_interrupts(-1, None, e)

                    try:
                        self.finish_project()
                        return SAVE_STATE_OK
                    
                    except Exception, e:
                        # To catch errors at interpreter shutdown
                        pass
                else:
                    logconsole('Could not re-run saved state, defaulting to standard configuration...')
                    self._cfg.resuming = False
                    return SAVE_STATE_NOT_OK
            else:
                logconsole('OK, falling back to default configuration...')
                return SAVE_STATE_NOT_OK
        else:
            return SAVE_STATE_NOT_OK
        pass

    def handle_interrupts(self, signum, frame, e=None):

        # print 'Signal handler called with',signum
        if signum == signal.SIGINT:
            if self._cfg.ignoreinterrupts:
                return False
            else:
                self._cfg.keyboardinterrupt = True

        if e != None:
            logconsole('Exception received=>',e)

        logtraceback()
        # dont allow to write cache, since it
        # screws up existing cache.
        GetObject('datamanager').conditional_cache_set()
        self.save_current_state()
        
        # sys.tracebacklimit = 0
        self.clean_up()
        
    def main(self):
        """ Main routine """

        # Set stderr to dummy to prevent all the thread
        # errors being printed by interpreter at shutdown
        # sys.stderr = DummyStderr()

        self.init()
        
        signal.signal(signal.SIGINT, self.handle_interrupts)
        
        # See if a crash file is there, then try to load it and run
        # program from crashed state.
        if self.run_saved_state() == SAVE_STATE_NOT_OK:
            # No such crashed state or user refused to run
            # from crashed state. So do the usual run.
            self.run_projects()
            
        # Final cleanup
        self.finalize()

if __name__=="__main__":
    spider = HarvestMan()
    spider.init_config()
    spider.main()
    

               
        


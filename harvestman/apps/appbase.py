# -- coding: utf-8
"""
appbase.py - Defines the base application class for
applications using the HarvestMan framework.

Author: Anand B Pillai <abpillai at gmail dot com>

Modification History

Created: Dec 12 2007       Anand B Pillai     By moving code from
                                              harvestman.py module.
"""

import sys, os
import __init__
import atexit

from lib import config
from lib import logger

from lib.common.common import *
from lib.common.methodwrapper import MethodWrapperMetaClass

class HarvestManAppBase(object):
    
    __metaclass__ = MethodWrapperMetaClass    
    
    def __init__(self):
        self.prepare()
        
    def prepare(self):
        """ Prepare """
        
        # Init Config Object
        InitConfig(config.HarvestManStateObject)
        # Initialize logger object
        InitLogger(logger.HarvestManLogger)
        
        self._cfg = GetObject('config')
        
    def process_plugins(self):
        """ Load all plugin modules """

        from lib import hooks
        
        plugin_dir = os.path.abspath(os.path.join(os.path.dirname(__init__.__file__), '..', 'ext'))
        # print plugin_dir

        if os.path.isdir(plugin_dir):
            sys.path.append(plugin_dir)
            # Load plugins specified in plugins list
            for plugin in self._cfg.plugins:
                # Load plugins
                try:
                    logconsole('Loading plugin %s...' % plugin)
                    M = __import__(plugin)
                    func = getattr(M, 'apply_plugin', None)
                    if not func:
                        logconsole('Invalid plugin %s, should define function "apply_plugin"!' % plugin)
                    try:
                        logconsole('Applying plugin %s...' % plugin)
                        func()
                    except Exception, e:
                        logconsole('Error while trying to apply plugin %s' % plugin)
                        logconsole('Error is:',str(e))
                        sys.exit(0)
                except KeyError, e:
                    logconsole('Error importing plugin module %s' % plugin)
                    logconsole('Error is:',str(e))
                    logconsole('Invalid plugin: %s !' % plugin)
                    hexit(0)


    def get_options(self):
        """ Read program options from command line or
        configuration files """
        
        # Get program options
        self._cfg.get_program_options()

    

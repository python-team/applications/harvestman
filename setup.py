from distutils.core import setup
from distutils.ccompiler import CCompiler, new_compiler

import os, sys
import zipfile
import shutil
import stat
import urllib
import tarfile

BUILD_SGMLOP=False

if os.name == 'posix':
    BUILD_SGMLOP = True
    
pyparsing_surl = 'http://cheeseshop.python.org/packages/source/p/pyparsing/pyparsing-1.4.8.tar.gz'

def site_packages_dir():
    
    for p in sys.path:
        if p.find('site-packages') != -1:
            return p
    
def make_data_files():
    data_files = []
    # Get the install directory
    sitedir = site_packages_dir()
    data_list = []
    
    # Create list for doc directory first.
    # harvestman doc dir
    if os.path.isdir("doc"):
        hdir = os.path.join(sitedir, "HarvestMan", "doc")
        for f in os.listdir("doc"):
            data_list.append("".join(("doc/",f)))
        data_files.append((hdir, data_list))

    return data_files

def locate_executable(name):

    path = os.environ.get('PATH')
    if os.name == 'posix':
        paths = path.split(':')
    elif os.name == 'nt':
        paths = path.split(';')
        
    for p in paths:
        exepath = os.path.join(p, name)
        if os.path.isfile(exepath):
            return exepath

    return ''
    
def install_pyparsing():

    print 'Installing pyparsing...'

    # Can't rely on easy_install being there ...
    easy_install = False # locate_executable('easy_install')
    flag = True
    
    if easy_install:
        try:
            import pkg_resources
            
            print 'Installing pyparsing using easy_install...'
            os.system(' '.join((easy_install, pyparsing_surl)))
            try:
                import pyparsing
                flag = False
            except ImportError, e:
                print e
            
        except ImportError:
            print 'easy_install is not setup correctly'
            
    if flag:
        print 'Installing directly from source...'
        temp_dir = os.path.join('deps','build_pyparsing')
        
        if not os.path.isdir(temp_dir):
            try:
                os.makedirs(temp_dir)
            except OSError, e:
                print e
                return -1

        fname = os.path.join(temp_dir, 'pyparsing.tar.gz')

        try:
            print 'Retrieving %s...' % pyparsing_surl
            urllib.urlretrieve(pyparsing_surl, fname)
        except Exception, e:
            print e
            return -1

        child_dir = ''
        
        if os.path.isfile(fname):
            print 'Extracting %s...' % fname
            try:
                f = tarfile.open(fname, 'r:gz')
                f.extractall(temp_dir)
                child_dir = os.path.join(temp_dir, os.listdir(temp_dir)[0])
            except tarfile.TarError, e:
                print e
                return -1
        else:
            print 'Error retrieving %s.' % pyparsing_surl
            return -1

        curdir = os.path.abspath(os.curdir)
        if child_dir:
            os.chdir(child_dir)
            ret = os.system('python setup.py install')
        else:
            print 'Error in extracting %s.' % fname
            return -1

        os.chdir(curdir)
        
        try:
            shutil.rmtree(temp_dir)
        except (IOError, OSError), e:
            print e

        return ret

    return -1
            

def create_shortcut(app):

    print 'Creating application link for %s...' % app
    
    sitedir = os.path.join(os.path.dirname(os.__file__), 'site-packages')
    prefix = sys.prefix
    exe_prefix = os.path.join(prefix, 'bin')
    
    app_path = os.path.join(exe_prefix, app)
    try:
        envp = "env" # os.path.join(exe_prefix, 'env')
        modpath = os.path.join(sitedir, 'harvestman', 'apps', app + '.py')
        string = '%s python %s $*' % (envp, modpath)
        open(app_path, 'w').write(string)
        # Make it executable...
        os.chmod(app_path, stat.S_IRWXU|stat.S_IRGRP|stat.S_IXGRP|stat.S_IROTH|stat.S_IXOTH)
        return 0
    except IOError, e:
        print e
        return -1
    
def check_sgmlop():

    html="""\
    <html><
    title>Test sgmlop</title>
    <body>
    <p>This is a pargraph</p>
    <img src="img.jpg"/>
    <a href="http://www.python.org'>Python</a>
    </body>
    </html>
    """
    
    try:
        import sgmlop
        print 'Found sgmlop...'
        
        class DummyHandler(object):
            links = []
            def finish_starttag(self, tag, attrs):
                self.links.append(tag)
                pass

        print 'Testing sgmlop...'
        parser = sgmlop.SGMLParser()
        parser.register(DummyHandler())
        parser.feed(html)
        # Check if we got all the links...
        if len(DummyHandler.links)==4:
            print 'tested sgmlop ok'
            return 0
        else:
            print 'sgmlop test failed, disabling sgmlop...'
            return -1
    except ImportError:
        return -1

def check_ccompiler():
    # Check if we can compile C programs, currently just instantiates
    # a compiler object... need to see if we should do further than
    # this...
    try:
        ccomp = new_compiler()
        return ccomp
    except Exception, e:
        print e
        sys.exit(1)
        
def build_sgmlop():

    print 'Building sgmlop C extension...'
    print '(Set the variable BUILD_SGMLOP to False and re-run the script if you want to disable sgmlop build)'
    fnames = os.listdir("deps")
    fname = [item for item in fnames if item.startswith('sgmlop')][0]
    if fname:
        fpath = os.path.join('deps', fname)
        # Should be a zipfile
        z = zipfile.ZipFile(fpath, 'r')
        print 'Extracting...'
        # Create directory...
        tempdir = os.path.join('deps', 'build_sgmlop')
        if not os.path.isdir(tempdir):
            try:
                os.makedirs(tempdir)
            except OSError, e:
                print e
                return -1
            
        for zinfo in z.infolist():
            arfile = zinfo.filename
            data = z.read(arfile)
            # Write with only filename
            arpath = os.path.join(tempdir, os.path.split(arfile)[-1])
            open(arpath, 'w').write(data)
            
        # Build sgmlop
        curdir = os.path.abspath(os.curdir)
        
        os.chdir(tempdir)
        ret = os.system("python setup.py install")

        os.chdir(curdir)
        
        try:
            shutil.rmtree(tempdir)
        except (IOError, OSError), e:
            print e
            
        return ret

    return -1
            
        
def version():
    vinfo = sys.version_info    
    return (vinfo, '.'.join((str(vinfo[0]), str(vinfo[1]), str(vinfo[2]))))

def main():
    vinfo, ver = version()
    print "Python version %s detected..." % ver
        
    if vinfo < (2,4):
        print 'Error: You need Python 2.4 or greater for HarvestMan'
        sys.exit(1)

    use_sgmlop = False
    
    if check_sgmlop()==-1:
        if BUILD_SGMLOP:
            print 'sgmlop not installed, checking for c compiler...'
            comp = check_ccompiler()
            if comp:
                print 'C compiler found...'
                # Build sgmlop...
                if build_sgmlop()==0 and check_sgmlop() != -1:
                    use_sgmlop = True
        else:
            print 'BUILD_SGMLOP flag set to False, not trying to build sgmlop...'
    else:
        use_sgmlop = True
            
    if use_sgmlop:
        print 'sgmlop will be used as the default HTML parser...'

    try:
        print 'Checking for pyparsing...'
        import pyparsing
        print 'found pyparsing.'
    except ImportError:
        print 'pyparsing not found.'
        if install_pyparsing() == -1:
            print 'Could not install pyparsing.'
        else:
            print 'Installed pyparsing successfully.'
        
    setup(name="HarvestMan",
          version="2.0alpha",
          description="A modular, extensible, flexible, multithreaded webcrawler application and framework",
          author="Anand B Pillai",
          author_email="abpillai_at_gmail_dot_com",
          maintainer="Anand B Pillai",
          maintainer_email="abpillai_at_gmail_dot_com",
          url="http://www.harvestmanontheweb.com",
          license="GNU General Public License v 2.0",
          requires=["Python (>=2.4)"],
          download_url="http://www.harvestmanontheweb.com/packages/2.0/HarvestMan-2.0alpha.tar.gz",
          classifiers=[
          'Development Status :: 5 - Stable',
          'Environment :: Console',
          'Environment :: Web Environment',
          'Intended Audience :: End Users/Desktop',
          'Intended Audience :: Developers',
          'License :: OSI Approved :: GNU General Public License',
          'Operating System :: OS Independent',
          'Programming Language :: Python',
          'Topic :: Offline browsing, Indexing, Search',
          ],

          package_dir = {'harvestman': 'harvestman'},
          packages = ['harvestman',
                      'harvestman.apps',
                      'harvestman.lib',
                      'harvestman.lib.common',
                      'harvestman.lib.js',
                      'harvestman.ext',
                      'harvestman.test',
                      'harvestman.dev'],
          )

    from harvestman.lib.config import HarvestManStateObject
    cfg = HarvestManStateObject()

    etcdir = cfg.etcdir
    print 'Creating basic configuration in %s...' % etcdir

    # If using sgmlop parser set htmlparser option to 1...
    conf_data = cfg.generate_system_configuration()

    if not os.path.isdir(etcdir):
        try:
            os.makedirs(etcdir)
        except OSError, e:
            print e
            sys.exit(1)

    if os.path.isdir(etcdir):
        etcfile = os.path.join(etcdir, 'config.xml')
        open(etcfile, 'w').write(conf_data)
        print 'done.'
    else:
        print 'Could not create system configuration!'

    if os.name == 'posix':
        # Create short-cuts
        print 'Creating application links...'
        create_shortcut("harvestman")
        create_shortcut("hget")        
        
if __name__ == "__main__":
    main()

